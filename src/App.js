
import logo from './assets/images/avatardefault_92824.png'

import { gUserInfo } from './info';


function App() {
  return (
    <div >
      <h5>{gUserInfo.firstname} {gUserInfo.lastname}</h5>

      <img src={logo} width={300} alt="avatar"/>

      <p>Age: {gUserInfo.age} </p>

      <p>{gUserInfo.age < 35 ? "Anh ấy còn trẻ" : "Anh ấy đã già" }</p>

      <ul>
        {
          gUserInfo.language.map((element, index) => {
            return <li key={index}>{element}</li>
          })
        }
      </ul>

    </div>
  );
}

export default App;
